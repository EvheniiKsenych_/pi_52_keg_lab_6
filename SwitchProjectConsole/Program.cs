﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchProjectConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            bool emmn = true;
            int a = 0, b = 0, c = 0;
            double a1, b1;
            do
            {
                Console.WriteLine("1. Розшифровка днiв тижня");
                Console.WriteLine("2. Розшифровка мiсяця");
                Console.WriteLine("3. Виконання операцiй над числами");
                Console.WriteLine("4. Вихiд");
                input = Console.ReadLine();
                a = int.Parse(input);
                switch (a)
                {
                    case 1:
                        {
                            Console.WriteLine("Введiть номер дня тижня");
                            input = Console.ReadLine();
                            b = int.Parse(input);
                            switch (b)
                            {
                                case 1:
                                    Console.WriteLine("Понедiлок");
                                    break;
                                case 2:
                                    Console.WriteLine("Вiвторок");
                                    break;
                                case 3:
                                    Console.WriteLine("Середа");
                                    break;
                                case 4:
                                    Console.WriteLine("Четвер");
                                    break;
                                case 5:
                                    Console.WriteLine("Пятниця");
                                    break;
                                case 6:
                                    Console.WriteLine("Субота");
                                    break;
                                case 7:
                                    Console.WriteLine("Недiля");
                                    break;
                                default:
                                    Console.WriteLine("Введено некоректне значення");

                                    break;
                            }
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Введiть номер мiсяця");
                            input = Console.ReadLine();
                            b = int.Parse(input);
                            switch (b)
                            {
                                case 1:
                                    Console.WriteLine("Сiчень");
                                    break;
                                case 2:
                                    Console.WriteLine("Лютий");
                                    break;
                                case 3:
                                    Console.WriteLine("Березень");
                                    break;
                                case 4:
                                    Console.WriteLine("Квiтень");
                                    break;
                                case 5:
                                    Console.WriteLine("Травень");
                                    break;
                                case 6:
                                    Console.WriteLine("Червень");
                                    break;
                                case 7:
                                    Console.WriteLine("Липень");
                                    break;
                                case 8:
                                    Console.WriteLine("Серпень");
                                    break;
                                case 9:
                                    Console.WriteLine("Вересень");
                                    break;
                                case 10:
                                    Console.WriteLine("Жовтень");
                                    break;
                                case 11:
                                    Console.WriteLine("Листопад");
                                    break;
                                case 12:
                                    Console.WriteLine("Грудень");
                                    break;
                                default:
                                    Console.WriteLine("Введено некоректне значення");
                                    break;
                            }
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("Введiть а:");
                            input = Console.ReadLine();
                            a1 = double.Parse(input);
                            Console.WriteLine("Введiть b:");
                            input = Console.ReadLine();
                            b1 = double.Parse(input);

                            Console.WriteLine("1. Додати");
                            Console.WriteLine("2. Помножити");
                            Console.WriteLine("3. Вiдняти");
                            Console.WriteLine("4. Подiлити");
                            input = Console.ReadLine();
                            c = int.Parse(input);
                            switch (c)
                            {
                                case 1:
                                    Console.WriteLine("A+B={0}", (a1 + b1).ToString());
                                    break;
                                case 2:
                                    Console.WriteLine("A*B={0}", (a1 * b1).ToString());
                                    break;
                                case 3:
                                    Console.WriteLine("A-B={0}", (a1 - b1).ToString());
                                    break;
                                case 4:
                                    Console.WriteLine("A/B={0}", (a1 / b1).ToString());
                                    break;
                                default:
                                    Console.WriteLine("Введено некоректне значення");
                                    break;
                            }
                            break;
                        }
                    case 4:
                        {
                            emmn = false;
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Помилка, вибраний неicнуючий пункт меню");
                            break;
                        }
                }
            }
            while (emmn);
        }
    }
}
