﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int xx, yy;
            double S, S1, S2, S3;
            int[] X = new int[3];
            int[] Y = new int[3];

            Console.WriteLine("Введiть координати трикутника: ");
            for (int i = 0; i < 3; i++)
            {
                Console.Write("Введiть X{0}: ", (i + 1));
                X[i] = int.Parse(Console.ReadLine());
                Console.Write("Введiть Y{0}: ", (i + 1));
                Y[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Введiть координати точки: ");

            Console.Write("Введiть X: ");
            xx = int.Parse(Console.ReadLine());
            Console.Write("Введiть Y: ");
            yy = int.Parse(Console.ReadLine());

            S = Sis(X[0], X[1], X[2], Y[0], Y[1], Y[2]);
            S1 = Sis(X[0], X[1], xx, Y[0],Y[1], yy);
            S2 = Sis(X[1], X[2], xx, Y[1], Y[2], yy);
            S3 = Sis(X[2], X[0], xx, Y[2], Y[0], yy);

            if(S==S1+S2+S3)
                Console.WriteLine("Точка належить трикутнику");
            else Console.WriteLine("Точка не належить трикутнику");
        }

        static double Sis(int x1, int x2, int x3, int y1, int y2, int y3)
        {
            double S=0;
            S += ((x1 + x2) * (y2 - y1));
            S += ((x2 + x3) * (y3 - y2));
            S += ((x3 + x1) * (y1 - y3));
            S /= 2;
            S = Math.Abs(S);
            return S;
        }
    }
}
