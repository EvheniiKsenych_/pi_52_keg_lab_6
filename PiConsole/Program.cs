﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PiConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int iter = 15;
            //Console.WriteLine("Viet({1}) = {0}", Viet(iter), iter);
            Console.WriteLine("Wallis({1}) = {0}", Wallis(iter), iter);
            Console.WriteLine("Brouncker({1}) = {0}", Brouncker(iter), iter);
            Console.WriteLine("Leibnic({1}) = {0}", Leibnic(iter), iter);

        }

        static double Viet(int iter)
        {
            double elementar1 = Math.Sqrt(1.0 / 2), tmp = (1.0/2)* Math.Sqrt(1 / 2), zr = elementar1 ;
            for(int i=1;i< iter;i++)
            {
                tmp = Math.Sqrt(tmp);
                zr += tmp;
                elementar1 *= zr;
            }
            return (2.0/elementar1);
        }

        static double Wallis(int iter)
        {
            double pi = 1;
            int dil = 2;
            for(int i=0;i< iter;i++)
            {
                pi *= (((dil - 1.0) * (dil + 1.0))/(dil*dil));
                dil += 2;
            }
            pi = 2 / pi;
            return pi;
        }

        static double Brouncker(int iter)
        {
            int znam = 5;
            double result = 9;
            double size = 2;
            for (int i = 1; i < iter; i++)
            {
                result = size + (result / (znam * znam));
                znam += 2;
                size = (int)Math.Log10(znam * znam) + 1;
            }
            return (4/(1 + (1 / result)));
        }


        static double Leibnic(int iter)
        {
            double pi = 0;
            int dil = 1;
            for (int i = 0; i < iter; i++)
            { 
                if((i+1)%2!=0)pi += (1.0/dil);
                else pi -= (1.0 / dil);
                dil += 2;  
            }
            return pi*4;
        }

    }
}
