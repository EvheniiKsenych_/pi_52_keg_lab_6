﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhileConsole1
{
    class Program
    {
        static void Main(string[] args)
        {
            string tmp="";
            int a, result = 0;
            do
            {
                tmp = Console.ReadLine();
                a = int.Parse(tmp);
                result += a;
            }
            while (a!=0);
            Console.WriteLine(result.ToString());
        }
    }
}
