﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileConsole2
{
    class Program
    {
        static void Main(string[] args)
        {
            double now = 10, all=10;
            string tmp;
            int p = 0, day=1;
            Console.WriteLine("Введiть % щоденного збiльшення: ");
            tmp = Console.ReadLine();
            p = int.Parse(tmp);
            while(true)
            {
                if (all > 200) break;
                now = (now*(100 + p)) / 100;
                all += now;
                day++;
            }
            Console.WriteLine("Сумарний пробiг = " + all.ToString());
            Console.WriteLine("Днiв потрачено = " + day.ToString());
        }
    }
}
