﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            double result = 0;
            Console.WriteLine("Введiть кiлькiсть вершин:");
            a = int.Parse(Console.ReadLine());
            int[] X = new int[a];
            int[] Y = new int[a];
            for(int i=0;i< a;i++)
            {
                Console.Write("Введiть X{0}: ", (i+1));
                X[i] = int.Parse(Console.ReadLine());
                Console.Write("Введiть Y{0}: ", (i + 1));
                Y[i] = int.Parse(Console.ReadLine());
            }
            for(int i=0;i< a-1;i++)
            {
                result += ((X[i]+X[i+1])*(Y[i+1]-Y[i]));
            }
            result += ((X[a-1] + X[0]) * (Y[0] - Y[a-1]));
            result /= 2;
            result = Math.Abs(result);
            Console.WriteLine("S= "+ result);
        }
    }
}
