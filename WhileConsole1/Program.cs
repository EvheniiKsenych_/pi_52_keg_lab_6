﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileConsole1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            string tmp;
            Console.WriteLine("Введiть N");
            tmp = Console.ReadLine();
            n = int.Parse(tmp);
            int i = 1;

            while (true)
            {
                if (Math.Pow(3, i) > n) break;
                i++;
            }
            
            Console.WriteLine(i.ToString());
        }
    }
}
